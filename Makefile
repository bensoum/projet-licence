.PHONY: clean help
.DEFAULT_GOAL= help

CC=pdflatex -shell-escape -interaction=nonstopmode -file-line-error
BIB = bibtex

help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-10s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

pdf: main.tex main.bbl ## build pdf output from the main file
	$(CC) main.tex -draftmode
	$(CC) main.tex 
	mv main.pdf memoire.pdf
main.bbl: main.aux 
	$(BIB) main.aux
main.aux: biblio.bib
	pdflatex main.tex -draftmode
	pdflatex main.tex -draftmode
draft: main.tex
	$(CC) main.tex 
	mv main.pdf memoire.pdf
clean: ## clean all build files
	@rm -f *.aux *.bak *.log *.out *.toc *.gz *.fls *.fdb_latexmk *.dvi *.nav *.snm *.vrb *.mtc* *.lot *.maf *.lof *.brf *.pdf *.bbl *.blg *.M* 
show: ## show pdf
	evince memoire.pdf
